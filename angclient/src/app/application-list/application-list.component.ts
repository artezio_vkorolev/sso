import { Component, OnInit } from '@angular/core';
import { ApplicationService } from '../service/application.service';
import { Application } from '../model/application';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-application-list',
    templateUrl: './application-list.component.html',
    styleUrls: ['./application-list.component.css']
})
export class ApplicationListComponent implements OnInit {
    data: Application[] = [];
    displayedColumns: string[] = ['applicationName', 'applicationDescription', 'applicationURL'];
    isLoadingResults = true;

    constructor(private applicationService: ApplicationService, private authService: AuthService, private router: Router) { }

    findAll(): void {
        this.applicationService.findAll().subscribe(applications => {
            this.data = applications;

            console.log(this.data);
            this.isLoadingResults = false;
        }, err => {
            console.log(err);
            this.isLoadingResults = false;
        });
    }

    logout() {
        localStorage.removeItem('token');
        this.router.navigate(['login']);
    }

    ngOnInit() {
        this.findAll();
    };
}


