export class Application {
    id: string;
    applicationName: string;
    applicationDescription: string;
    applicationURL: string;
}
