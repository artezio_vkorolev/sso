import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Application } from '../model/application';
import { catchError, tap } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})

export class ApplicationService {
    private applicationUrl: string;

    constructor(private http: HttpClient) {
        this.applicationUrl = 'http://localhost:8080/applications';
    }

    findAll(): Observable<Application[]> {
        return this.http.get<Application[]>(this.applicationUrl)
            .pipe(
                tap(_ => this.log('fetched Applications')),
                catchError(this.handleError('findAll', []))
            );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            console.error(error); // log to console instead
            this.log(`${operation} failed: ${error.message}`);

            return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}

