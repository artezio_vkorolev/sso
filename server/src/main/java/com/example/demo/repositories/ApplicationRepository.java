package com.example.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.entities.Application;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {

    @Query("select a from Application a where a.applicationName = :name")
    Application findByName(@Param("name") String name);
    void delete (Application entity);
}
