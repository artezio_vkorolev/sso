package com.example.demo.service;

import org.springframework.security.core.userdetails.UserDetails;

import com.example.demo.entities.Role;
import com.example.demo.entities.User;


public interface UserDetailsService {
    User findByUserName(String name);
    Role findByRoleName(String name);
    UserDetails loadUserByUsername(String email);
}
