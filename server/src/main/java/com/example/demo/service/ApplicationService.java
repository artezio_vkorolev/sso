package com.example.demo.service;

import java.util.List;

import com.example.demo.entities.Application;

public interface ApplicationService {

    List<Application> getApplications();
    Application getApplicationByName(String name);
    //Application addApplication(Application application);
}
