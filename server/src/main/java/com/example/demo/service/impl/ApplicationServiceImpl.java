package com.example.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.entities.Application;
import com.example.demo.repositories.ApplicationRepository;
import com.example.demo.service.ApplicationService;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    @Autowired
    private ApplicationRepository repository;

    @Override
    public Application getApplicationByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public List<Application> getApplications() {
        return repository.findAll();
    }
    /*
    @Override
    public Application addApplication(Application application) {
        return repository.save(application);
    }
*/
}