package com.example.demo.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "applications")
public class Application implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Application_Id", nullable = false)
    private Long id;

    @Column(name = "Application_Name", unique = true, nullable = false)
    private String applicationName;

    @Column(name = "Application_Description", nullable = false)
    private String applicationDescription;

    @Column(name = "Application_URL", nullable = false)
    private String applicationURL;

    public Application() {
    }

    public Application(String name, String description, String url) {
        this.applicationName = name;
        this.applicationDescription = description;
        this.applicationURL = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationDescription() {
        return applicationDescription;
    }

    public void setApplicationDescription(String applicationDescription) {
        this.applicationDescription = applicationDescription;
    }

    public String getApplicationURL() {
        return applicationURL;
    }

    public void setApplicationURL(String applicationURL) {
        this.applicationURL = applicationURL;
    }

    @Override
    public String toString() {
        return "Application [id=" + id + ", applicationName=" + applicationName + ", applicationDescription="
                + applicationDescription + ", applicationURL=" + applicationURL + "]";
    }
}
