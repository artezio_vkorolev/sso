package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import com.example.demo.entities.Application;
import com.example.demo.repositories.ApplicationRepository;
import com.example.demo.service.ApplicationService;
import com.example.demo.service.impl.ApplicationServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationServiceImplIntegrationTests {

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {

        @Bean
        public ApplicationService applicationService() {
            return new ApplicationServiceImpl();
        }
    }

    @Autowired
    private ApplicationService applicationService;

    @MockBean
    private ApplicationRepository applicationRepository;

    @Before
    public void setUp() {
        Application app = new Application("App3", "Description App3", "Application_URL");
        Mockito.when(applicationRepository.findByName(app.getApplicationName())).thenReturn(app);
    }

    @Test
    public void whenValidName_thenApplicationShouldBeFound() {
        String name = "App3";
        Application found = applicationService.getApplicationByName(name);
        assertThat(found.getApplicationName()).isEqualTo(name);
    }
}
