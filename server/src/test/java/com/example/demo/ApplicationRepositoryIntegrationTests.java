package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import com.example.demo.entities.Application;
import com.example.demo.repositories.ApplicationRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ApplicationRepositoryIntegrationTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ApplicationRepository applicationRepository;

    // write test cases here
    @Test
    public void whenFindByName_thenReturnApplication() {
        // given
        Application app = new Application();
        app.setApplicationName("App3");
        app.setApplicationDescription("DescriptionApp3");
        app.setApplicationURL("http://localhost:8080/app3/");
        entityManager.persist(app);
        entityManager.flush();

        // when
        Application found = applicationRepository.findByName(app.getApplicationName());

        // then
        assertThat(found.getApplicationName()).isEqualTo(app.getApplicationName());
    }
}