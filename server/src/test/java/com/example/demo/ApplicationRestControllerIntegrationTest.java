package com.example.demo;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.example.demo.entities.Application;
import com.example.demo.repositories.ApplicationRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class ApplicationRestControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ApplicationRepository repository;

    @After
    public void resetDb() {
        repository.deleteAll();
    }

    @Test
    public void givenApplications_whenGetApplications_thenStatus200() throws Exception {

        createTestApplication("App3", "AppDescription", "localhost:8080");

        mvc.perform(get("/api/applications").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[2].applicationName", is("App3")));
    }

    private void createTestApplication(String applicationName, String applicationDescription, String applicationURL) {
        Application app = new Application(applicationName, applicationDescription, applicationURL);
        repository.saveAndFlush(app);
    }
}
