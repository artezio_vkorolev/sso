package com.example.demo;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.demo.config.WebSecurityConfig;
import com.example.demo.controllers.ApplicationController;
import com.example.demo.entities.Application;
import com.example.demo.service.ApplicationService;
import com.example.demo.service.impl.ApplicationServiceImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(value = {ApplicationServiceImpl.class})
@ContextConfiguration(classes = {ApplicationController.class, WebSecurityConfig.class})
public class ApplicationControllerTests {

    @Autowired
    private MockMvc mvc;
    
    @MockBean
    private ApplicationService service;
    
    @Before
    public void setUp() throws Exception {
    }
    
    @Test
    public void givenApplications_whenGetApplications_thenReturnJsonArray()
      throws Exception {
         
        Application app = new Application("App3", "Description App3", "Application_URL");
        
        List< Application> allApplications = Arrays.asList(app);
        
        given(service.getApplications()).willReturn(allApplications);
     
        mvc.perform(get("/api/applications")
          .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$", hasSize(1)))
          .andExpect(jsonPath("$[0].applicationName", is(app.getApplicationName())));
    }
}
